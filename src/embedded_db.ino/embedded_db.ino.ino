
String username = "admin";
String password = "dGhlc2UgYXJlIG5vdCB0aGUgY2hhcmFjdGVycyB5b3UgYXJlIGxvb2tpbmcgZm9y";

int r1 = 0x5a; 
int r2 = 0x6d; 
int r3 = 0x78; 
int r4 = 0x68; 
int r5 = 0x5a; 
int r6 = 0x33; 
int r7 = 0x74; 
int r8 = 0x54; 
int r9 = 0x56; 
int r10 = 0x44; 
int r11 = 0x46; 
int r12 = 0x4d; 
int r13 = 0x54; 
int r14 = 0x46; 
int r15 = 0x39; 
int r16 = 0x56; 
int r17 = 0x55; 
int r18 = 0x7a; 
int r19 = 0x46; 
int r20 = 0x4f; 
int r21 = 0x4e; 
int r22 = 0x6c; 
int r23 = 0x39; 
int r24 = 0x54; 
int r25 = 0x56; 
int r26 = 0x44; 
int r27 = 0x51; 
int r28 = 0x33; 
int r29 = 0x53; 
int r30 = 0x55; 
int r31 = 0x4e; 
int r32 = 0x66; 
int r33 = 0x55; 
int r34 = 0x44; 
int r35 = 0x51; 
int r36 = 0x31; 
int r37 = 0x4e; 
int r38 = 0x56; 
int r39 = 0x64; 
int r40 = 0x50; 
int r41 = 0x55; 
int r42 = 0x6b; 
int r43 = 0x52; 
int r44 = 0x54; 
int r45 = 0x66; 
int r46 = 0x51; 
int r47 = 0x3d; 
int r48 = 0x3d;

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data

boolean newData = false;

void setup() {
  Serial.begin(9600);
  Serial.println("<Arduino is ready>");
}

void loop() {
  recvWithEndMarker();
  showNewData();
}

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
  
  // if (Serial.available() > 0) {
           while (Serial.available() > 0 && newData == false) {
  rc = Serial.read();
  
  if (rc != endMarker) {
    receivedChars[ndx] = rc;
    ndx++;
    if (ndx >= numChars) {
    ndx = numChars - 1;
    }
  }
  else {
    receivedChars[ndx] = '\0'; // terminate the string
    ndx = 0;
    newData = true;
    }
  }
}

void showNewData() {
  if (newData == true) {
    Serial.print("This just in ... ");
    Serial.println(receivedChars);
    newData = false;
  }
}

void printHex(int num, int precision=2) {
  char tmp[16];
  char format[128];
  
  sprintf(format, "%%.%dX", precision);
  
  sprintf(tmp, format, num);
  Serial.print(tmp);
}

void showHelp() {
  printHex(r1);
  printHex(r2);
  printHex(r3);
  printHex(r4);
  printHex(r5);
  printHex(r6);
  printHex(r7);
  printHex(r8);
  printHex(r9);
  printHex(r10);
  printHex(r11);
  printHex(r12);
  printHex(r13);
  printHex(r14);
  printHex(r15);
  printHex(r16);
  printHex(r17);
  printHex(r18);
  printHex(r19);
  printHex(r20);
  printHex(r21);
  printHex(r22);
  printHex(r23);
  printHex(r24);
  printHex(r25);
  printHex(r26);
  printHex(r27);
  printHex(r28);
  printHex(r29);
  printHex(r30);
  printHex(r31);
  printHex(r32);
  printHex(r33);
  printHex(r34);
  printHex(r35);
  printHex(r36);
  printHex(r37);
  printHex(r38);
  printHex(r39);
  printHex(r40);
  printHex(r41);
  printHex(r42);
  printHex(r43);
  printHex(r44);
  printHex(r45);
  printHex(r46);
  printHex(r47);
  printHex(r48);

  Serial.println();
  delay(1000);
}
