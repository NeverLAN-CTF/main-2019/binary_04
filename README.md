# Binary 4

#### Description
It appears we're not doing a very good job at employee retention and statisfaction. Now our Embedded Firmawre Engineer has left and removed the source code for our latest IoT firmware. Please help us to recover the source code for the following hex file. We would prefer C, but assembly would work as well. I also think there's a flag somewhere in the sorce for you.

#### Flag
flag{ST1LL_US1N6_ST47IC_P455WORDS}

#### Hints
Some assembly required

#### Solution
* convert the hex to assembly
* avr-objdump -j .sec1 -d -m avr5 embedded_db.hex > embedded_db.asm
* step through the code to find the hex values being loaded to the registers
* convert the hex values to ascii => base 64 string
* convert base 64 string to ascii
* avr-objdump -D -m avr5 embedded_db.hex | grep ldi | cut -d',' -f2 | cut -d' ' -f2 | tr -d ';' | tr -d '\n' | sed 's/0x0/0/g' | sed 's/0x//g' | tr -d '\t' | xxd -r -ps
